from setuptools import find_packages, setup

setup (
    name='stockApi',
    version='1.0.0',
    packages=['stockApi'],
    zip_safe=False,
    install_requires=[
        'flask',
    ],
)
