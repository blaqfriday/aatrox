from flask import Blueprint
from flask import request
import stockApi.StockAPI as sapi
import json

stockSearcher = sapi.StockAPI()

routes_api = Blueprint('routes_api', __name__)
def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise

@routes_api.route('/')
@routes_api.route('/stockdata')
def show_default():
    default_message = json.dumps({'response':'default api response'})
    return default_message

@routes_api.route('/stockdata/ticker', methods=['GET'])
def get_daily_ticker():
    parameters = request.args
    ticker = parameters.get('ticker')
    start_day = parameters.get('start_day')
    end_day= parameters.get('end_day')
    data = []
    if start_day != None and end_day != None:
        data = stockSearcher.get_stocks_in_range(ticker, start_day, end_day)
    else:
        data = stockSearcher.get_stocks(ticker)
    return data

@routes_api.route('/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'
