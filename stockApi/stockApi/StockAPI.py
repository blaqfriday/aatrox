from alpha_vantage.timeseries import TimeSeries
import matplotlib.pyplot as plt
import time
import logging
import os
import json

_DAY = 86400
_MONTH = 2592000
_YEAR = 31556952
_CLOSE = "4. close"
_DATE = "date"

class StockAPI:
    _API_KEY_TEST = ""

    def __init__(self):
        self.timeseries = TimeSeries(key=self._API_KEY_TEST, output_format='json')
        logging.basicConfig(level=os.environ.get("LOGLEVEL", "WARNING"))

    # default function that will get last 24 hours of stock data
    def get_stocks(self, indexes):
        #TODO: add case of more than one stock
        index = indexes
        if isinstance(index, list):
            index = indexes[0]
        try:
            data, meta_data = self.timeseries.get_daily(symbol=index, outputsize='compact')
        except ValueError:
            return self._errorPayload(index)

        payload = self._createJsonPayload(index, data)
        return payload

    def get_stocks_in_range(self, indexes, start_day="", end_day=""):
        if start_day == "" and end_day == "":
            return self.get_stocks(indexes)


    def get_stocks_with_extra(self, indexes, start_day="", end_day=""):
        if start_day == "" and end_day == "":
            return self.get_stocks(indexes)
        interval = self._determine_Interval(start_day, end_day)
        start_day, end_day =  self._find_extension(start_day, end_day)
        return get_stocks_in_range(indexes, start_day, end_day)


    def _determine_interval(self, start_day, end_day):
        if end_day < start_day:
            logging.error("end time is before start time")
        timeDifference = end_day - start_day
        if timeDifference > _MONTH:
            return "60min"
        if timeDifference >_MONTH/2:
            return "30min"
        if timeDifference > _DAY*7:
            return  "15min"
        if timeseries > _DAY:
            return "5min"
        return "1min"

    def _find_extension(self, start_day, end_day):
        if end_day < start_day:
            logging.error("end time is before start time")
        return start_day, end_day
        #TODO: implement the below
        # if timeDifference > _MONTH:
        #     return start - _MONTH/2,
        # if timeDifference >_MONTH/2:
        #     return "30min"
        # if timeDifference > _DAY*7:
        #     return  "15min"
        # if timeseries > _DAY:
        #     return "5min"
        # return "1min"

    # def _deleteWeekends(stockdata):
    def _createJsonPayload(self, ticker, stock_data):
        close_values = {key: stock_data[key][_CLOSE] for key in stock_data.keys()}
        payload = json.dumps({
        "ticker": ticker,
        "close_values": close_values
        })
        return payload

    def _errorPayload(self, ticker):
        payload = json.dumps({
            "bad_ticker": ticker
        })
        return payload
