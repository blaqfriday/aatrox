import os
from flask import Flask
from stockApi.routes import routes_api


app = Flask(__name__, instance_relative_config=True)
app.register_blueprint(routes_api)
