import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div>
      <Container>
        <StockChart ticker="MSFT"/>
      </Container>
    </div>
  );
}

export default App;
