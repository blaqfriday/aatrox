package twitter

import (
	b64 "encoding/base64"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	cl "aatrox/client"
)

const (
	ConsumerKey       = "ASK TIM FOR THIS"
	ConsumerSecret    = "ASK TIM FOR THIS TOO"
	TwitterOAuth2     = "https://api.twitter.com/oauth2/token"
  	UserTweetsEP 	  = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name={username}&count=2"
)

type Twitter struct {
	client           cl.Client
	consumerKey      string
	consumerSecret   string
}

func CreateTwitterClient() Twitter {
	log.Println("Twitter Client called")
	twitter := Twitter{}
	twitter.client = cl.CreateClient()
	//twitter.consumerKey, twitter.consumerSecret = getConsumerTokens()
	// create a key that contains our oauth tokens
	encodedKeySecret := b64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s",
														url.QueryEscape(ConsumerKey),
														url.QueryEscape(ConsumerSecret))))

	// Authentication Routine
	err := twitter.setAccessToken(encodedKeySecret)
	if err != nil {
		log.Fatal(err)
	}
	defer twitter.client.EndClient()
	return twitter
}

func (tw *Twitter) setAccessToken(secret string) error{
	// Authentication Routine
	requestBody := cl.CreateBuffer(`grant_type=client_credentials`)

	err := tw.client.PostRequest(TwitterOAuth2, requestBody)
	if err != nil {
		return err
	}
	tw.client.AddHeader("Authorization", fmt.Sprintf("Basic %s", secret))
	tw.client.AddHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	tw.client.AddHeader("Content-Length", strconv.Itoa(requestBody.Len()))

	err = tw.client.Do()
	if err != nil {
		return err
	}

	tw.client.SaveToken()
	if err != nil {
		return err
	}

	return nil
}

func (tw *Twitter) GetRecentUserTweets(username string) ([]byte, error){
	endpoint := UserTweetsEP
	endpoint = strings.Replace(endpoint, "{username}", username, 1)
	err := tw.client.GetRequest(endpoint)
	if err != nil {
		return []byte("") , err
	}

	tw.client.AddAuthorization()
	err = tw.client.Do()
	//defer tw.client.EndClient()
	if err != nil {
		return []byte(""), err
	}
	log.Printf("Retrieved %s tweets", username)
	return tw.client.GetBody()
}