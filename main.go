package main

import (
  "context"
  "flag"
  "fmt"
  tw "aatrox/twitter"
  "github.com/gorilla/mux"
  "log"
  "net/http"
  "os"
  "os/signal"
  "time"
)

var twitter tw.Twitter

func Home(w http.ResponseWriter, r *http.Request) {
  w.WriteHeader(http.StatusOK)
}

func GetTweets(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  w.WriteHeader(http.StatusOK)
  username := vars["username"]
  tweets, err := twitter.GetRecentUserTweets(username)
  if err != nil {
  log.Println(err)
  }
  fmt.Fprint(w,  string(tweets))
}

func main() {
  twitter = tw.CreateTwitterClient()
  var wait time.Duration
  flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
  flag.Parse()

  router := mux.NewRouter()

  server := &http.Server{
    Addr: "0.0.0.0:8085",
    WriteTimeout: time.Second*15,
    ReadTimeout: time.Second*15,
    IdleTimeout: time.Second*60,
    Handler: router,
  }

  // start a sub routine for the server
  go func() {
    router.HandleFunc("/", Home)
    router.HandleFunc("/tweets/{username}", GetTweets).Methods("GET")
    http.Handle("/", router)
    if err := server.ListenAndServe(); err != nil {
      log.Println(err)
    }
  }()

  channel := make(chan os.Signal, 1)
  signal.Notify(channel, os.Interrupt)

  // block until we get a signal
  <-channel

  ctx, cancel := context.WithTimeout(context.Background(), wait)
  defer cancel()
  server.Shutdown(ctx)
  log.Println("shutting down")
  os.Exit(0)
}
