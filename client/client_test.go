package client

import (
  "testing"
  "reflect"
)

func Test_InitClient(t *testing.T) {
  cases := []struct{
    testClient Client
    expectedClient Client
  }{
    {
      expectedClient: Client{
        name: nil,
        baseClient: &http.Client{},
        response: &http.Response{},
        request: &http.Request{},
      },
    },
  }

  for _, c := range(cases) {
    c.testClient.InitClient()
    if !reflect.DeepEqual(c.testClient, c.expectedClient) {
      t.Error("Client was not initialized to expected value")
    }
  }
}
