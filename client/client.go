package client

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
)

type Client struct {
    baseClient *http.Client
    response *http.Response
    request *http.Request
    bearer BearerToken
}

const (
  GET = "GET"
  POST = "POST"
)

func CreateClient() Client{
  cl := Client{}
  cl.baseClient = &http.Client{}
  cl.response = &http.Response{}
  cl.request = &http.Request{}
  cl.bearer = BearerToken{}
  return cl
}

type BearerToken struct {
    AccessToken string `json:"access_token"`
}

func (cl *Client) PostRequest(endPoint string, body *bytes.Buffer) error {
    var err error
    cl.request, err = http.NewRequest(POST, endPoint, body)
    return err
}

func (cl *Client) GetRequest(endPoint string) error {
    var err error
    cl.request, err = http.NewRequest(GET, endPoint, nil)
    return err
}

func (cl *Client) Do() error {
    var err error
    cl.response, err = cl.baseClient.Do(cl.request)
    return err
}

func (cl *Client) GetBody() ([]byte, error) {
    return ioutil.ReadAll(cl.response.Body)
}

func (cl *Client) AddHeader(key string, value string) {
  cl.request.Header.Add(key, value)
}

func (cl *Client) AddAuthorization() {
    cl.request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", cl.bearer.AccessToken))
}

func (cl *Client) EndClient() {
  cl.response.Body.Close()
  cl.request.Body.Close()
}

// saves current response as bearer token
func (cl *Client) SaveToken() error {
    responseBody, err := ioutil.ReadAll(cl.response.Body)
    json.Unmarshal(responseBody, &cl.bearer)
    return err
}

func CreateBuffer(input string) *bytes.Buffer{
    return bytes.NewBuffer([]byte(input))
}
